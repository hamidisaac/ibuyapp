import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:ibuymarket/ShoppingBasketData.dart';
import 'package:ibuymarket/models/category_item.dart';
import 'package:ibuymarket/pages/splash_screen.dart';
import 'package:ibuymarket/shoppingBasket.dart';
import 'ShopBottomNavigator.dart';
import 'Product.dart';
import 'DescriptionPage.dart';
import 'Branches.dart';
import 'dart:convert';

import 'home/sub/get_home_categories_list.dart';

void main() => runApp(MainMaterial());

class MainMaterial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        theme: ThemeData(
            // Use the old theme but apply the following three changes
            textTheme: Theme.of(context).textTheme.apply(
                  fontFamily: 'IranSans',
                )),
        routes: {
          '/': (context) => new Directionality(
              textDirection: TextDirection.rtl, child: new SplashScreen()),
          '/store': (context) => new Store(),
        });
  }
}

class Store extends StatefulWidget {
  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store>   with TickerProviderStateMixin {
  List<Product> _items = [];
  TabController _controller;
  @override
  void initState() {
    super.initState();
    fetchItems();
    _controller = TabController(
        length: 3, initialIndex: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {

    var CategoryList = List<Category>();
    CategoryList.add(Category(id: 1, name: 'برنج' ,imageUrl : 'assets/images/rice_logo.png' , bgImageUrl : 'assets/images/rice.png'));
    CategoryList.add(Category(id: 2, name: 'پروتئین' ,imageUrl : 'assets/images/protein_logo.png' , bgImageUrl : 'assets/images/protein.png'));
    CategoryList.add(Category(id: 3, name: 'چای' ,imageUrl : 'assets/images/tea_logo.png' , bgImageUrl : 'assets/images/tea.png'));
    CategoryList.add(Category(id: 4, name: 'روغن' ,imageUrl : 'assets/images/oil_logo.png' , bgImageUrl : 'assets/images/oil.png'));
    CategoryList.add(Category(id: 5, name: 'قند و شکر' ,imageUrl : 'assets/images/sugar_logo.png' , bgImageUrl : 'assets/images/sugar.png'));
    CategoryList.add(Category(id: 6, name: 'حبوبات' ,imageUrl : 'assets/images/beans_logo.png' , bgImageUrl : 'assets/images/beans.png'));


    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(double.infinity, 80), // 44 is the height
          child: Directionality(
            textDirection: TextDirection.rtl,
            child: Stack(
              children: <Widget>[
                Container(
                  color: Color(0xFF16c5d0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding:
                              EdgeInsets.only(top: 20.0, left: 10.0, right: 15.0),
                          child: TextField(
                            style: TextStyle(color: Color(0xFFff6c00)),
                            cursorColor: Color(0xFFff6c00),
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.go,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(45.0)),
                                borderSide: BorderSide.none,
                              ),
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 15),
                              hintText: "چه چیزی نیاز دارید؟",
                              hintStyle: TextStyle(
                                  fontSize: 14.0, color: Color(0xFFff6c00)),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: new EdgeInsets.only(
                            top: 35.0, left: 20.0, bottom: 20.0),
                        child: Image.asset(
                          'assets/images/logo.png',
                          fit: BoxFit.contain,
                          height: 100,
                          width: 75,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Directionality(
          textDirection: TextDirection.rtl,
          child: ListView(
            children: <Widget>[
              Container(
                color: Color(0xFF16c5d0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding:
                          new EdgeInsets.only(left: 15, right: 15, bottom: 10),
                      child: Image.asset(
                        'assets/images/promo_image.png',
                        height: 150,
                        width: MediaQuery.of(context).size.width - 30,
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  GetHomeCategoriesList(
                      list: CategoryList
                  ),
          TabBar(
              labelStyle: TextStyle(
                fontSize: 15,
                fontFamily: 'IranSans',
                ),
              labelColor: Colors.grey,
              controller: _controller,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorColor: Color(0xFF16c5d0),
              indicatorWeight: 3,
              indicatorPadding: EdgeInsets.fromLTRB(-20, 0, -20,-2),
              tabs: [
                Tab(
                  text: 'جدیدترین',
                ),
                Tab(
                  text: "پرفروش ترین",
                ),
                Tab(
                  text: "محبوب ترین",
                ),
              ]),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.grey.withOpacity(0.3))
                      )
                    ),
                  ),
                ],
              ),
              GridView.count(
                padding: EdgeInsets.symmetric(vertical: 5,horizontal: 15),
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                childAspectRatio:MediaQuery.of(context).size.height / 800,
                mainAxisSpacing: 10,
                physics: ScrollPhysics(),
                // to disable GridView's scrolling
                shrinkWrap: true,
                children: List.generate(_items.length, (int position) {
                  return generateItem(_items[position], context);
                }),
              ),
            ],
          ),
        ),
        bottomNavigationBar: ShopBottomNavigator(),
      ),
    );
  }

  void fetchItems() async {
    var url = 'https://ibuymarket.ir/api/products?ws_key=EWQQSP2HLKD5ACY1UCL3WSMTB4BX7H17&display=full&output_format=JSON&limit=10';
    Response response = await get(url);
    if(response.statusCode == 200) {
      setState(() {
        var productJson = jsonDecode(utf8.decode(response.bodyBytes));
        for (var i in productJson['products']) {
          var productItem = Product(
              i['name'],
              i['id'],
              i['price'].split('.')[0],
              i['description'],
              i['description_short'],
              i['id_manufacturer'],
              i['id_default_image'],
              i['quantity'],
              i['reference'],
              i['meta_description']);
          _items.add(productItem);
        }
      });
    }
  }
}

Container generateItem(Product product, context) {
  return Container(
    decoration: BoxDecoration(
    color: Colors.white,
      borderRadius: BorderRadius.circular(5),
      boxShadow: [

        BoxShadow(
            blurRadius: 10,
            offset: Offset(0,5),
            color: Color(0x29242424),
            spreadRadius: 0)
      ],
    ),
    height: 250,
    child: InkWell(
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => DescriptionPage(product)));
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Center(
              child: Container(
                margin: EdgeInsets.only(top: 5,left: 10,right: 10),
                height: 120,
                child: FadeInImage.assetNetwork(
                  image: "https://ibuymarket.ir/api/images/products/${product.id}/${product.id_default_image}?ws_key=EWQQSP2HLKD5ACY1UCL3WSMTB4BX7H17",
                  fit: BoxFit.contain,
                  placeholder: 'assets/images/default-product.png',
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10,left: 10,bottom: 2),
            child: Text(
              product.name,
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 13),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10,left: 10,bottom: 2),
            child: Text(
              '500 گرم',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 11),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(color: Color(0xFF16c5d0) )
            ),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(right: 7),
                    child: Text(
                      product.price,
                      style: TextStyle(color: Color(0xFF16c5d0), fontSize: 14.0),
                    ),
                  ),
                ),
                Container(
                  color: Color(0xFF16c5d0),
                  padding: EdgeInsets.symmetric(vertical: 4,horizontal: 15),
                  child: Text('خرید',style: TextStyle(color: Colors.white),),
                )
              ],
            ),
          )
        ],
      ),
    ),
  );
}
