import 'package:flutter/material.dart';
import 'package:ibuymarket/ShoppingBasketData.dart';
import 'package:ibuymarket/ShoppingBasketItem.dart';
import 'package:ibuymarket/pages/login.dart';
import 'package:ibuymarket/profile.dart';

import 'main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShoppingBasket extends StatefulWidget {
  @override
  _ShoppingBasketState createState() => _ShoppingBasketState();
}

class _ShoppingBasketState extends State<ShoppingBasket> {
  @override
  var _name = '';
  var _family = '';
  var _mobile = '';
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          body: BasketUI(),
          bottomNavigationBar: BottomAppBar(
            clipBehavior: Clip.antiAlias,
            child: Container(
              height: 120,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('مبلغ قابل پرداخت',
                            style: TextStyle(
                              fontSize: 12,
                              color: Color(0xFF929292),
                            ),
                          ),
                          Text('۰ تومان ',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color(0xFF16c5d0),
                            ),
                          ),
                        ],
                      ),
                      GestureDetector(
                        onTap: (){
                          print(ShoppingBasketData.getInstance().basketItem.length);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xFF16c5d0),
                              borderRadius: BorderRadius.all(Radius.circular(10))
                          ),
                          child: Center(
                            child: Text(
                              'ثبت نهایی',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          width: 150,
                          height: 50,
                        ),
                      )
                    ],
                  ),
                  Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width / 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(
                              Icons.home,
                              color: Colors.blueGrey[600],
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width / 3,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[Text('سبد خرید')],
                      ),
                    ),
                    InkWell(
                      onTap: () async {
                        if(await checkLogin()) {
                        Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ProfileScreen(mobile : _mobile, name : _name, family : _family)
                        ));
                        } else {
                        Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => LoginScreen()
                        ));
                        }
                      },
                      child: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width / 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(
                              Icons.person_outline,
                              color: Colors.blueGrey[600],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
              ),
            ),
          )),
    );
  }

  Widget BasketUI() {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 10),
          child: ListView.builder(
            itemCount: ShoppingBasketData.getInstance().basketItem.length,
            itemBuilder: (context, position) {
              return GestureDetector(
                child: Padding(
                    padding: EdgeInsets.only(left: 15, right: 10, top: 10),
                    child: ShoppingBasketItem(
                        ShoppingBasketData.getInstance().basketItem[position],
                        removeItem,
                        position)),
              );
            },
          ),
        ),
      ],
    );
  }

  void removeItem(int index) {
    setState(() {
      ShoppingBasketData.getInstance().basketItem.removeAt(index);
    });
  }
  Future<bool> checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getString('user.token') == null) {
      return false;
    } else {
      _name = prefs.getString('user.name');
      _family = prefs.getString('user.family');
      _mobile = prefs.getString('user.mobile');
      return true;
    }
  }
}
