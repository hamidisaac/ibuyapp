import 'package:flutter/material.dart';

class InputFieldArea extends StatelessWidget {
  final String hint;
  final bool obscure;
  final IconData icon;

  InputFieldArea({this.hint , this.obscure , this.icon});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      margin: const EdgeInsets.only(bottom: 20),
      decoration: new BoxDecoration(
        border: new Border(
          bottom: new BorderSide(
            width: 0.5,
            color: Colors.black
          )
        )
      ),
      child: new TextFormField(
        textDirection: TextDirection.ltr,
        keyboardType: TextInputType.number,
        obscureText: obscure,
        style: const TextStyle(
          color: Colors.black
        ),
        decoration: new InputDecoration(
          icon: new Icon(
            icon ,
            color: Colors.black,
          ),
          border: InputBorder.none,
          hintText: hint,
          hintStyle: const TextStyle(color: Color(0xFFdddddd), fontSize: 18),
          contentPadding: const EdgeInsets.only(
            top: 15 , right: 0 , bottom: 20 , left: 5
          )
        ),
      ),
    );
  }

}