import 'package:flutter/material.dart';
import 'package:ibuymarket/components/InputFields.dart';

class FormContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: new Column(
        children: <Widget>[
          SizedBox(height: 50,),
          new Text('ورود شماره موبایل', style: new TextStyle(fontSize: 26),),
          new Form(
            child: new Column(
              children: <Widget>[
                new InputFieldArea(
                  hint: "**** *** **09",
                  obscure: false,
                ),
              ],
            ),
          ),
          new Text('برای ورود شماره موبایل خود را وارد نمایید، یک کد ۵ رقمی از طریق پیامک برای شما ارسال خواهد شد',
          style: new TextStyle(fontSize: 13),),
        ],
      ),
    );
  }

}