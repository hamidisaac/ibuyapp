class Product{
  String _name;
  int _id;
  String _price;
  String _description;
  String _description_short;
  String _id_manufacturer;
  String _id_default_image;
  String _quantity;
  String _reference;
  String _meta_description;

  Product(this._name, this._id, this._price, this._description,
      this._description_short, this._id_manufacturer, this._id_default_image,
      this._quantity, this._reference, this._meta_description);

  String get description => _description;

  String get description_short => _description_short;
  String get id_manufacturer => _id_manufacturer;
  String get id_default_image => _id_default_image;
  String get quantity => _quantity;

  String get price => _price;
  String get reference => _reference;
  String get meta_description => _meta_description;

  int get id => _id;

  String get name => _name;
}