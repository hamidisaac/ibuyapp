import 'package:flutter/material.dart';
import 'Product.dart';

typedef OnRemovePressed(int index);

class ShoppingBasketItem extends StatefulWidget {
  Product _product;
  int _count = 0;
  int _index;
  OnRemovePressed _onRemovePressed;
  ShoppingBasketItem(this._product, this._onRemovePressed, this._index);

  @override
  _ShoppingBasketItemState createState() => _ShoppingBasketItemState();
}

class _ShoppingBasketItemState extends State<ShoppingBasketItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        height: 120,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          textDirection: TextDirection.rtl,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 10, left: 10),
              child: Image.network(
                "https://ibuymarket.ir/api/images/products/${widget._product.id}/${widget._product.id_default_image}?ws_key=EWQQSP2HLKD5ACY1UCL3WSMTB4BX7H17",
                width: 100,
                height: 100,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 10, right: 20),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 150, maxHeight: 20),
                      child: Container(
                        child: Text(
                          widget._product.name,
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 5, right: 0, left: 50),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: 150, maxHeight: 20),
                      child: Container(
                        child: Text(
                          widget._product.price + ' تومان ',
                          style: TextStyle(
                            color: Color(0xFF16c5d0),
                            fontSize: 14,
                          ),
                        ),
                      ),
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 40, bottom: 30),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: 100),
                    child: Container(
                      color: Color(0xFF0f000000),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          GestureDetector(
                            child: Icon(
                              Icons.add,
                              size: 18,
                            ),
                            onTap: () {
                              Increment();
                            },
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(widget._count.toString()),
                          SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.remove,
                              size: 18,
                            ),
                            onTap: () {
                              Decrement();
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(top: 50, bottom: 20, left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            widget._onRemovePressed(widget._index);
                          },
                          child: Icon(Icons.delete_outline, color: Color(0xFFff3d00),)),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void Increment() {
    setState(() {
      widget._count++;
    });
  }

  void Decrement() {
    setState(() {
      if (widget._count == 0) {
        return;
      } else {
        widget._count--;
      }
    });
  }
}
