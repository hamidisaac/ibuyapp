import 'package:flutter/material.dart';
import 'package:ibuymarket/components/Form.dart';
import 'package:http/http.dart' as http;
import 'package:ibuymarket/pages/register.dart';
import 'dart:convert';

class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

  final _scaffoldKey = GlobalKey<ScaffoldState>();

class _LoginState extends State<LoginScreen> {
  @override
  PageController _pageController;
  TextEditingController _textController;

  var _regForm = false;
  var _name = '';
  var _family = '';
  @override
  void initState() {
    _pageController = PageController(keepPage: true, initialPage: 2);
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _textController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    // TODO: implement build

    TextEditingController mobileController = new TextEditingController();
    var page = MediaQuery.of(context).size;

    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: new Container(
          child: new Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(bottom: 300.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/images/login_logo.png'),
                  ],
                ),
                width: page.width,
                height: page.height,
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  image: AssetImage("assets/images/background.png"),
                  fit: BoxFit.cover,
                )),
              ),
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 20,),
                  new Text('ورود شماره موبایل', style: new TextStyle(fontSize: 26),),
                  new Container(
                    height: 50,
                    margin: const EdgeInsets.only(top:10, left: 30, right: 30, bottom: 20),
                    decoration: new BoxDecoration(
                        border: Border.all(color: Color(0xFFdddddd)),
                        borderRadius: BorderRadius.all(Radius.circular(4))
                    ),
                    child: new Column(
                      children: <Widget>[
                        TextField(
                          maxLength: 11,
                          controller: _textController, //Add controller to your TextField
                          obscureText: false,
                          textAlign: TextAlign.center,
                          textDirection: TextDirection.rtl,
                          keyboardType: TextInputType.number,
                          style: const TextStyle(
                              color: Colors.black,
                            fontSize: 18
                          ),
                          decoration: new InputDecoration(
                              counterText: '',
                              border: InputBorder.none,
                              hintText: '**** *** **۰۹',
                              hintStyle: const TextStyle(color: Color(0xFFdddddd), fontSize: 18),
                              contentPadding: const EdgeInsets.all(5)
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    height: 50,
                    margin: const EdgeInsets.only(left: 30, right: 30),
                  child:
                  new Text('برای ورود شماره موبایل خود را وارد نمایید، یک کد ۶ رقمی از طریق پیامک برای شما ارسال خواهد شد',
                    style: new TextStyle(fontSize: 13),),
                  )
                ],
              ),
              GestureDetector(
                onTap: () async {
                  if(_textController.text == ''){
                    FocusScope.of(context).previousFocus();
                  } else {
                    if(_textController.text.length < 11) {
                      FocusScope.of(context).previousFocus();
                    } else {
                      var httpCode = await sendDataForLogin(_textController.text);
                      if(httpCode == 200) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context)=> RegisterScreen(mobile : _textController.text, regForm : _regForm, name : _name, family : _family)
                        ));
                      } else {
                        _scaffoldKey.currentState.showSnackBar(
                            new SnackBar(
                              content: new Text(
                                  'ارسال پیامک با خطا مواجه شد'
                              ),
                            )
                        );
                      }
                    }
                  }
                },
                child: new Container(
                  margin: const EdgeInsets.only(bottom: 30),
                  width: 320,
                  height: 50,
                  alignment: Alignment.center,
                  decoration: new BoxDecoration(
                      color: new Color(0xff16c5d0),
                      borderRadius:
                          new BorderRadius.all(const Radius.circular(4))),
                  child: new Text(
                    "مرحله بعد",
                    style: new TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w300,
                        letterSpacing: .3),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future sendDataForLogin(mobile) async {
      /*return await post(Uri.encodeFull('https://ibuymarket.ir/module/ibuymarket/ibuylogin'), body: {'ajax':'1', 'action':'send-sms', 'phone':mobile.toString()}, headers: {"Content-Type":"application/json", "Accept":"application/json"})
          .then((Response response) {
              print(response.body);
        final int statusCode = response.statusCode;
        return response.statusCode;
      });*/
    var client = http.Client();
    try {
      var response = await client.post('https://ibuymarket.ir/modules/ibuymarket/api/index.php',
        body: {'ajax':'1', 'action':'send-sms', 'phone':mobile.toString()},
      );
      var pointsJson = jsonDecode(response.body);
      print(pointsJson);
      _regForm = pointsJson['register_form'];
      _name = pointsJson['name'];
      _family = pointsJson['family'];
      return response.statusCode;
    } finally {
      client.close();
    }
    /*final response = await http.post('https://ibuymarket.ir/module/ibuymarket/ibuylogin', body: {'ajax':'1', 'action':'send-sms', 'phone':mobile.toString()}, headers: {"Accept":"application/json"});
    var pointsJson = jsonDecode(response.body);
    _regForm = pointsJson['register_form'];
    _name = pointsJson['name'];
    _family = pointsJson['family'];
    return response.statusCode;*/
  }
}
