import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ibuymarket/profile.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:requests/requests.dart';

class RegisterScreen extends StatefulWidget {
  @override
  final String mobile;
  final String name;
  final String family;
  final bool regForm;
  RegisterScreen({this.mobile, this.regForm, this.name, this.family});

  _RegisterState createState() => _RegisterState();
}

  final _scaffoldKey = GlobalKey<ScaffoldState>();

class _RegisterState extends State<RegisterScreen> {
  @override
  PageController _pageController;
  TextEditingController _codeController;
  TextEditingController _nameController;
  TextEditingController _familyController;

  var _name = '';
  var _family = '';
  @override
  void initState() {
    _pageController = PageController(keepPage: true, initialPage: 2);
    _codeController = TextEditingController();
    _nameController = TextEditingController();
    _familyController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _codeController.dispose();
    _nameController.dispose();
    _familyController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    // TODO: implement build

    TextEditingController mobileController = new TextEditingController();
    var page = MediaQuery.of(context).size;

    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: new Container(
          child: new Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(bottom: 400.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/images/login_logo.png'),
                  ],
                ),
                width: page.width,
                height: page.height,
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                  image: AssetImage("assets/images/background.png"),
                  fit: BoxFit.cover,
                )),
              ),
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 100,),
                  new Text('کد تایید', style: new TextStyle(fontSize: 26),),
                  new Container(
                    height: 50,
                    margin: const EdgeInsets.only(top:0, left: 30, right: 30, bottom: 5),
                    decoration: new BoxDecoration(
                        border: Border.all(color: Color(0xFFdddddd)),
                        borderRadius: BorderRadius.all(Radius.circular(4))
                    ),
                    child: new Column(
                      children: <Widget>[
                        TextField(
                          maxLength: 6,
                          controller: _codeController, //Add controller to your TextField
                          obscureText: false,
                          textAlign: TextAlign.center,
                          textDirection: TextDirection.rtl,
                          keyboardType: TextInputType.number,
                          style: const TextStyle(
                              color: Colors.black,
                            fontSize: 18
                          ),
                          decoration: new InputDecoration(
                              counterText: '',
                              border: InputBorder.none,
                              hintText: '------',
                              hintStyle: const TextStyle(color: Color(0xFFdddddd), fontSize: 18),
                              contentPadding: const EdgeInsets.all(5)
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    height: 20,
                    margin: const EdgeInsets.only(left: 30, right: 30),
                  child:
                  new Text('کد دریافتی ۶ رقمی از طریق پیامک را وارد نمایید',
                    style: new TextStyle(fontSize: 13),),
                  ),
                  SizedBox(height: 10,),
                  Visibility(
                      visible: widget.regForm,
                      child : new Text('مشخصات', style: new TextStyle(fontSize: 26),)
                  ),
                  Visibility(
                    visible: widget.regForm,
                    child : new Container(
                    height: 50,
                    margin: const EdgeInsets.only(top:0, left: 30, right: 30, bottom: 10),
                    decoration: new BoxDecoration(
                        border: Border.all(color: Color(0xFFdddddd)),
                        borderRadius: BorderRadius.all(Radius.circular(4))
                    ),
                    child: new Column(
                      children: <Widget>[
                        TextField(
                          maxLength: 6,
                          controller: _nameController, //Add controller to your TextField
                          obscureText: false,
                          textAlign: TextAlign.center,
                          textDirection: TextDirection.rtl,
                          keyboardType: TextInputType.text,
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 18
                          ),
                          decoration: new InputDecoration(
                              counterText: '',
                              border: InputBorder.none,
                              hintText: 'نام',
                              hintStyle: const TextStyle(color: Color(0xFFdddddd), fontSize: 18),
                              contentPadding: const EdgeInsets.all(5)
                          ),
                        ),
                      ],
                    ),
                  )
                  ),
                  Visibility(
                    visible: widget.regForm,
                    child : new Container(
                    height: 50,
                    margin: const EdgeInsets.only(top:0, left: 30, right: 30, bottom: 10),
                    decoration: new BoxDecoration(
                        border: Border.all(color: Color(0xFFdddddd)),
                        borderRadius: BorderRadius.all(Radius.circular(4))
                    ),
                    child: new Column(
                      children: <Widget>[
                        TextField(
                          maxLength: 6,
                          controller: _familyController, //Add controller to your TextField
                          obscureText: false,
                          textAlign: TextAlign.center,
                          textDirection: TextDirection.rtl,
                          keyboardType: TextInputType.text,
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 18
                          ),
                          decoration: new InputDecoration(
                              counterText: '',
                              border: InputBorder.none,
                              hintText: 'نام خانوادگی',
                              hintStyle: const TextStyle(color: Color(0xFFdddddd), fontSize: 18),
                              contentPadding: const EdgeInsets.all(5)
                          ),
                        ),
                      ],
                    ),
                  )
                  ),
                ],
              ),
              GestureDetector(
                onTap: () async {
                  if(_codeController.text == ''){
                    FocusScope.of(context).previousFocus();
                  } else {
                    if(_codeController.text.length < 6) {
                      FocusScope.of(context).previousFocus();
                    } else {
                      var httpCode = await sendDataForRegister(widget.mobile.toString(), _codeController.text, _nameController.text, _familyController.text);
                      if(httpCode == 200) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context)=> ProfileScreen(mobile : widget.mobile.toString(), name : _name, family : _family)
                        ));
                      } else {
                        _scaffoldKey.currentState.showSnackBar(
                            new SnackBar(
                              content: new Text(
                                  'کد وارد شده صحیح نمی باشد'
                              ),
                            )
                        );
                      }
                    }
                  }
                },
                child: new Container(
                  margin: const EdgeInsets.only(bottom: 30),
                  width: 320,
                  height: 50,
                  alignment: Alignment.center,
                  decoration: new BoxDecoration(
                      color: new Color(0xff16c5d0),
                      borderRadius:
                          new BorderRadius.all(const Radius.circular(4))),
                  child: new Text(
                    "ورود",
                    style: new TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w300,
                        letterSpacing: .3),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future sendDataForRegister(mobile, code, name, family) async {
    _name = name;
    _family = family;
    if(_name == '')
      _name = widget.name.toString();
    if(_family == '')
      _family = widget.family.toString();
    var client = http.Client();
    try {
      var r = await client.post('https://ibuymarket.ir/modules/ibuymarket/api/index.php',
        body: {'ajax':'1', 'action':'verify-sms', 'phone':mobile.toString(), 'code':code.toString(), 'name':name.toString(), 'family':family.toString()},
      );
      var pointsJson = jsonDecode(r.body);
      if(r.statusCode == 200) {
        if(await storeUserData(pointsJson['token']) == true) {
          return r.statusCode;
        } else {
          return 500;
        }
      } else {
        return r.statusCode;
      }
    } finally {
      client.close();
    }
    /*String url = 'https://ibuymarket.ir/modules/ibuymarket/api/index.php';
    var r = await Requests.post(
        url,
        body: {'ajax':'1', 'action':'verify-sms', 'phone':mobile.toString(), 'code':code.toString(), 'name':name.toString(), 'family':family.toString()},
        bodyEncoding: RequestBodyEncoding.FormURLEncoded
    );
    r.raiseForStatus();
    dynamic json = r.json();
    if(r.statusCode == 200) {
      if(await storeUserData(json.token) == true) {
        return r.statusCode;
      } else {
        return 500;
      }
    } else {
      return r.statusCode;
    }*/
  }
  storeUserData(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user.token', token);
    prefs.setString('user.mobile', widget.mobile.toString());
    prefs.setString('user.name', _name);
    prefs.setString('user.family', _family);
    return true;
  }
}
