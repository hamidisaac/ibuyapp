import 'package:flutter/material.dart';
import 'package:mapir_gl/mapir_gl.dart';

class MapScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new MapScreenState();
}
final LatLng center = const LatLng(35.708329, 51.409836);

class MapScreenState extends State<MapScreen> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(title: const Text('Mapir Map')),
      body: MapirMap(
          initialCameraPosition: CameraPosition(target: center, zoom: 12)),
    );
  }
}
