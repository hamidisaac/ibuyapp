import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ibuymarket/ShoppingBasketData.dart';
import 'Product.dart';
import 'package:flutter_html/flutter_html.dart';

class DescriptionPage extends StatelessWidget {
  Product _product;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DescriptionPage(this._product);

  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: new Column(
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Center(
                    child: Image.network(
                      "https://ibuymarket.ir/api/images/products/${_product.id}/${_product.id_default_image}?ws_key=EWQQSP2HLKD5ACY1UCL3WSMTB4BX7H17",
                      height: 200,
                      width: 375,
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Align(
                    child: Padding(
                      padding: EdgeInsets.only(right: 16),
                      child: Directionality(
                        textDirection: TextDirection.rtl,
                        child: Text(
                          _product.name,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20
                          ),
                        ),
                      ),
                    ),
                    alignment: Alignment.topRight,
                  ),
                  /*SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.blueAccent)
                        ),
                        child: Text('نوع'),
                      ),
                      Text('102'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text('201'),
                      Text('202'),
                    ],
                  ),*/
                  SizedBox(
                    height: 20,
                  ),
                  Align(
                    child: Padding(
                      padding: EdgeInsets.only(right: 16),
                      child: Directionality(
                        textDirection: TextDirection.rtl,
                        child: Text(
                          'ویژگی های محصول',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20
                          ),
                        ),
                      ),
                    ),
                    alignment: Alignment.topRight,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 45, right: 45),
                    child: Html(
                      data: _product.description,
                      defaultTextStyle: TextStyle(fontSize: 15),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          clipBehavior: Clip.antiAlias,
          child: Container(
            height: 100,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('قیمت',
                      style: TextStyle(
                        fontSize: 12,
                        color: Color(0xFF929292),
                      ),
                    ),
                    Text(' تومان ' + _product.price,
                      style: TextStyle(
                        fontSize: 18,
                        color: Color(0xFF16c5d0),
                      ),
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: (){
                    _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        content: new Text(
                          'محصول به سبد خرید اضافه شد'
                        ),
                      )
                    );
                    ShoppingBasketData.getInstance().basketItem.add(_product);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color(0xFF16c5d0),
                        borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Center(
                      child: Text(
                        'افزودن به سبد خرید',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    width: 150,
                    height: 50,
                  ),
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}
