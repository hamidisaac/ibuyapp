import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibuymarket/models/category_item.dart';

class CategoryItem extends StatelessWidget{
  final Category category;

  CategoryItem({this.category });

  @override
  Widget build(BuildContext context) {

    return  InkWell(
      onTap: () {},
      child: Container(
          height: 99,
          width: 57,
          margin: EdgeInsets.all( 5),
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Positioned(
                bottom: 0,
                top: 15,
                right: 0,
                left: 0,
                child :Container(
                  decoration: BoxDecoration(
                    borderRadius: new BorderRadius.circular(7.0),
                    image: DecorationImage(
                      image: AssetImage(category.bgImageUrl),
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                top: 0,
                right: 0,
                left: 0,
                child :Image.asset(
                  category.imageUrl,
                  scale: 1.4,
                  alignment: Alignment(0, -1.1),
                ),
              ),
              Positioned(
                bottom: 0,
                top: 0,
                right: 0,
                left: 0,
                child :Center(

                child: Text(category.name,
                    style: TextStyle(
                        color: Color(0xFFffffff),
                        fontSize: 12.0)),
              )
              ),
//              Image.asset(
//                category.imageUrl,
//                scale: 1.5,
//                alignment: Alignment(0, -1.0),
//              ),
//
            ],
          )),
    );
  }

}