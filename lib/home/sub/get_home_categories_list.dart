
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ibuymarket/home/sub/category_item.dart';
import 'package:ibuymarket/models/category_item.dart';

class GetHomeCategoriesList extends StatelessWidget{
  final List<Category> list;

  GetHomeCategoriesList({Key key,this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return

      Column(
      children: [
        Row(
          children: [
            Container(
                padding: EdgeInsets.only(right: 15,top: 15,bottom: 5),
                child: Text(
                  'دسته بندی',
                  style: TextStyle(fontSize: 16),
                )
            )
          ],
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children:
              list.map((category) => CategoryItem(category: category)).toList(),
            ),
          )
        ),


      ],
    );
  }

}