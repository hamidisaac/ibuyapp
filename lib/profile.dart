import 'package:flutter/material.dart';
import 'package:ibuymarket/ShoppingBasketData.dart';
import 'package:ibuymarket/shoppingBasket.dart';

class ProfileScreen extends StatefulWidget {
  @override
  final String mobile;
  final String name;
  final String family;
  ProfileScreen({this.mobile, this.name, this.family});

  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          body: ProfileUI(),
          bottomNavigationBar: BottomAppBar(
            clipBehavior: Clip.antiAlias,
            child: Container(
              height: 70,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width / 3,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.home,
                                color: Colors.blueGrey[600],
                              )
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ShoppingBasket()));
                        },
                        child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width / 3,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.shopping_cart,
                                color: Colors.blueGrey[600],
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width / 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[Text('حساب کاربری')],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Widget ProfileUI() {
    final double circleRadius = 120.0;
    var page = MediaQuery.of(context).size;
    return DecoratedBox(
      decoration: BoxDecoration(),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Container(
              height: 120,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Container(
                      width: circleRadius,
                      height: circleRadius,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 8.0,
                            offset: Offset(0.0, 5.0),
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Icon(Icons.person),

                        /// replace your image with the Icon
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 10, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 150, maxHeight: 30),
                            child: Container(
                              child: Text(
                                widget.name + ' ' + widget.family,
                                style: TextStyle(
                                  fontSize: 22,
                                ),
                              ),
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.only(top: 0, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 150, maxHeight: 20),
                            child: Container(
                              child: Text(
                               widget.mobile,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: page.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 0.0),
                    child: Container(
                      width: circleRadius,
                      height: circleRadius,
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: new IconButton(
                            icon: new Icon(Icons.location_on, color: Colors.cyan),onPressed: null
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 0, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 150, maxHeight: 30),
                            child: Container(
                              child: Text(
                                'آدرس ها',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 15, left :20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                              },
                              child: Icon(Icons.arrow_forward_ios),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 50,
              width: page.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 0.0),
                    child: Container(
                      width: circleRadius,
                      height: circleRadius,
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: new IconButton(
                            icon: new Icon(Icons.favorite_border, color: Colors.cyan),onPressed: null
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 0, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 150, maxHeight: 30),
                            child: Container(
                              child: Text(
                                'مورد علاقه ها',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 15, left :20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                              },
                              child: Icon(Icons.arrow_forward_ios),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 50,
              width: page.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 0.0),
                    child: Container(
                      width: circleRadius,
                      height: circleRadius,
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: new IconButton(
                            icon: new Icon(Icons.access_time, color: Colors.cyan),onPressed: null
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 0, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 150, maxHeight: 30),
                            child: Container(
                              child: Text(
                                'سفارشات قبلی',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 15, left :20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                              },
                              child: Icon(Icons.arrow_forward_ios),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 50,
              width: page.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 0.0),
                    child: Container(
                      width: circleRadius,
                      height: circleRadius,
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: new IconButton(
                            icon: new Icon(Icons.folder_open, color: Colors.cyan),onPressed: null
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 0, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 200, maxHeight: 30),
                            child: Container(
                              child: Text(
                                'پیگیری سفارش',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 15, left :20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                                onTap: () {
                                },
                              child: Icon(Icons.arrow_forward_ios),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 50,
              width: page.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 0.0),
                    child: Container(
                      width: circleRadius,
                      height: circleRadius,
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: new IconButton(
                            icon: new Icon(Icons.ring_volume, color: Colors.cyan),onPressed: null
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 0, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 200, maxHeight: 30),
                            child: Container(
                              child: Text(
                                'پشتیبانی',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 15, left :20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                              },
                              child: Icon(Icons.arrow_forward_ios),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 50,
              width: page.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 0.0),
                    child: Container(
                      width: circleRadius,
                      height: circleRadius,
                      child: Padding(
                        padding: EdgeInsets.all(0.0),
                        child: new IconButton(
                            icon: new Icon(Icons.exit_to_app, color: Colors.cyan),onPressed: null
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 0, right: 0),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 200, maxHeight: 30),
                            child: Container(
                              child: Text(
                                'خروج از حساب کاربری',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
