import 'package:flutter/material.dart';
import 'package:ibuymarket/pages/login.dart';
import 'package:ibuymarket/profile.dart';
import 'package:ibuymarket/shoppingBasket.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShopBottomNavigator extends StatelessWidget {
  @override
  var _name = '';
  var _family = '';
  var _mobile = '';
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Colors.white,
      clipBehavior: Clip.antiAlias,
      child: Container(
        height: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width / 3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('فروشگاه')
                ],
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context)=> ShoppingBasket()
                ));
              },
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width / 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(Icons.shopping_cart, color: Colors.blueGrey[600],)
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                if(await checkLogin()) {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ProfileScreen(mobile : _mobile, name : _name, family : _family)
                  ));
                } else {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => LoginScreen()
                  ));
                }
              },
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width / 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(Icons.person_outline, color: Colors.blueGrey[600],)
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<bool> checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getString('user.token') == null) {
      return false;
    } else {
      _name = prefs.getString('user.name');
      _family = prefs.getString('user.family');
      _mobile = prefs.getString('user.mobile');
      return true;
    }
  }
}
